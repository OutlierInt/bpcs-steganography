var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BinaryGreyConverter = /** @class */ (function () {
    function BinaryGreyConverter() {
    }
    BinaryGreyConverter.bin2grey = function (b) {
        //Source: Steganography in Lossy and Lossless Images, pg. 16
        return b ^ (b >> 1);
    };
    BinaryGreyConverter.grey2bin = function (g) {
        //Source: Steganography in Lossy and Lossless Images, pg. 16
        var bin = 0;
        while (g !== 0) {
            bin ^= g;
            g >>= 1;
        }
        return bin;
    };
    BinaryGreyConverter.binImage2greyImage = function (imgData) {
        for (var i = 0; i < imgData.data.length; i += 4) {
            imgData.data[i + 0] = BinaryGreyConverter.bin2grey(imgData.data[i + 0]);
            imgData.data[i + 1] = BinaryGreyConverter.bin2grey(imgData.data[i + 1]);
            imgData.data[i + 2] = BinaryGreyConverter.bin2grey(imgData.data[i + 2]);
            //imgData[i + 3] = 255; //Don't change the alpha value.
        }
    };
    BinaryGreyConverter.greyImage2binImage = function (imgData) {
        for (var i = 0; i < imgData.data.length; i += 4) {
            imgData.data[i + 0] = BinaryGreyConverter.grey2bin(imgData.data[i + 0]);
            imgData.data[i + 1] = BinaryGreyConverter.grey2bin(imgData.data[i + 1]);
            imgData.data[i + 2] = BinaryGreyConverter.grey2bin(imgData.data[i + 2]);
            //imgData[i + 3] = 255; //Don't change the alpha value.
        }
    };
    return BinaryGreyConverter;
}());
var BitPlaneExtractor = /** @class */ (function () {
    function BitPlaneExtractor() {
    }
    BitPlaneExtractor.ExtractBitPlane = function (imgData, channel, bitNumber) {
        var bitplane = new BitPlane(imgData.width, imgData.height);
        var data = imgData.data;
        var mask = 1 << (7 - bitNumber);
        for (var i = channel, curPixel = 0; i < data.length; i += 4, curPixel++) {
            if ((data[i] & mask) !== 0) {
                bitplane.setPixelLinear(curPixel);
            }
        }
        return bitplane;
    };
    return BitPlaneExtractor;
}());
var BitPlaneComplexityDetector = /** @class */ (function () {
    function BitPlaneComplexityDetector() {
    }
    BitPlaneComplexityDetector.AnalyzeLocalComplexity = function (bitplane, blockX, blockY, squareSize, threshold) {
        //Max Horizontal complexity = (<column count> - 1) * <row count>
        //Max Vertical complexity = (<row count> - 1) * <column count>
        //Max complexity of NxN block = 2 * (N-1) * N
        //Get a block of the image
        var size = squareSize;
        var maxcomplexity = 2 * (size - 1) * size;
        blockX *= size;
        blockY *= size;
        //Get horizontal complexity
        var hComplexity = 0;
        for (var row = 0; row < size; row++) {
            var curColor = bitplane.getPixelAt(blockX + 0, blockY + row);
            for (var col = 1; col < size; col++) {
                var nextcolor = bitplane.getPixelAt(blockX + col, blockY + row);
                if (curColor !== nextcolor)
                    hComplexity++;
                curColor = nextcolor;
            }
        }
        //Get vertical complexity
        var vComplexity = 0;
        for (var col = 0; col < size; col++) {
            var curColor = bitplane.getPixelAt(blockX + col, blockY + 0);
            for (var row = 1; row < size; row++) {
                var nextcolor = bitplane.getPixelAt(blockX + col, blockY + row);
                if (curColor !== nextcolor)
                    vComplexity++;
                curColor = nextcolor;
            }
        }
        var complexity = (hComplexity + vComplexity) / maxcomplexity;
        return complexity;
    };
    BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap = function (bitplane, squareSize, threshold) {
        var gridWidth = Math.floor(bitplane.width / squareSize);
        var gridHeight = Math.floor(bitplane.height / squareSize);
        var complexityMap = [];
        for (var x = 0; x < gridWidth; x++) {
            for (var y = 0; y < gridHeight; y++) {
                var localComplexity = this.AnalyzeLocalComplexity(bitplane, x, y, squareSize, threshold);
                if (localComplexity >= threshold) {
                    complexityMap.push({ x: x, y: y, complexity: localComplexity });
                }
            }
        }
        return { squareSize: squareSize, minComplexity: threshold, complexityMap: complexityMap };
    };
    return BitPlaneComplexityDetector;
}());
var BitPlaneCanvasGenerator = /** @class */ (function () {
    function BitPlaneCanvasGenerator() {
    }
    BitPlaneCanvasGenerator.createBitPlaneCanvasObjects = function (bitplane, clearColor, setColor, elementBefore) {
        var bitplaneCanvas = document.createElement("canvas");
        bitplaneCanvas.width = bitplane.width;
        bitplaneCanvas.height = bitplane.height;
        if (!elementBefore) {
            //document.body.appendChild(bitplaneCanvas);
            tempStorage.appendChild(bitplaneCanvas);
        }
        else {
            elementBefore.parentElement.insertBefore(bitplaneCanvas, elementBefore.nextSibling);
        }
        var bitplaneCtx = bitplaneCanvas.getContext("2d");
        var bitplaneData = this.createBitPlaneDrawing(bitplane, clearColor, setColor);
        bitplaneCtx.putImageData(bitplaneData, 0, 0);
        return { bitplaneData: bitplaneData, bitplaneCanvas: bitplaneCanvas, bitplaneCtx: bitplaneCtx, clearColor: clearColor, setColor: setColor };
    };
    BitPlaneCanvasGenerator.createBitPlaneDrawing = function (bitPlane, clearColor, setColor) {
        //Image Data has 4 8-bit channels (RGBA).
        //Split the image into 24 1-bit images.
        var imgData = ctx.createImageData(bitPlane.width, bitPlane.height);
        var data = imgData.data;
        for (var i = 0, px = 0; i < data.length; i += 4, px++) {
            var bitSet = bitPlane.getPixelLinear(px);
            if (bitSet) {
                data[i + 0] = setColor[0];
                data[i + 1] = setColor[1];
                data[i + 2] = setColor[2];
            }
            else {
                data[i + 0] = clearColor[0];
                data[i + 1] = clearColor[1];
                data[i + 2] = clearColor[2];
            }
            data[i + 3] = 255;
        }
        return imgData;
    };
    return BitPlaneCanvasGenerator;
}());
//Represents a bit plane using an byte array
var BitPlane = /** @class */ (function () {
    function BitPlane(width, height) {
        this.width = width;
        this.height = height;
        this.data = new Uint8ClampedArray(Math.ceil(width * height / 8));
    }
    BitPlane.prototype.boundsCheck = function (x, y) {
        if (x < 0)
            throw new Error("X is less than zero!");
        if (y < 0)
            throw new Error("Y is less than zero!");
        if (x >= this.width)
            throw new Error("X is too large!");
        if (y >= this.height)
            throw new Error("Y is too large!");
    };
    BitPlane.prototype.getBitIndex = function (x, y) {
        this.boundsCheck(x, y);
        return y * (this.width) + x;
    };
    BitPlane.prototype.getByteIndex = function (x, y) {
        this.boundsCheck(x, y);
        return Math.floor(this.getBitIndex(x, y) / 8);
    };
    BitPlane.prototype.getBitPosition = function (x, y) {
        this.boundsCheck(x, y);
        var bitNumber = this.getBitIndex(x, y);
        var byteNumber = this.getByteIndex(x, y);
        return bitNumber - (byteNumber * 8);
    };
    BitPlane.prototype.getCoordsFromLinear = function (n) {
        var y = Math.floor(n / this.width);
        var x = n - (y * this.width);
        return { x: x, y: y };
    };
    BitPlane.prototype.getPixelLinear = function (n) {
        var coords = this.getCoordsFromLinear(n);
        return this.getPixelAt(coords.x, coords.y);
    };
    BitPlane.prototype.getPixelAt = function (x, y) {
        this.boundsCheck(x, y);
        var curByte = this.data[this.getByteIndex(x, y)];
        var bitPosition = this.getBitPosition(x, y);
        var bitTest = curByte & (1 << (7 - bitPosition));
        return bitTest !== 0 ? 1 : 0;
    };
    BitPlane.prototype.setPixelAt = function (x, y) {
        this.boundsCheck(x, y);
        var byteIndex = this.getByteIndex(x, y);
        var bitPosition = this.getBitPosition(x, y);
        var prevData = this.data[byteIndex];
        this.data[byteIndex] |= (1 << (7 - bitPosition));
        //console.log(`${prevData.toString(2).padStart(8, "0")} -> ${this.data[byteIndex].toString(2).padStart(8, "0")}`);
    };
    BitPlane.prototype.setPixelLinear = function (n) {
        var coords = this.getCoordsFromLinear(n);
        return this.setPixelAt(coords.x, coords.y);
    };
    BitPlane.prototype.clearPixelAt = function (x, y) {
        this.boundsCheck(x, y);
        var byteIndex = this.getByteIndex(x, y);
        var bitPosition = this.getBitPosition(x, y);
        var prevData = this.data[byteIndex];
        this.data[byteIndex] &= ~(1 << (7 - bitPosition));
        //console.log(`${prevData.toString(2).padStart(8, "0")} -> ${this.data[byteIndex].toString(2).padStart(8, "0")}`);
    };
    BitPlane.prototype.clearPixelLinear = function (n) {
        var coords = this.getCoordsFromLinear(n);
        return this.clearPixelAt(coords.x, coords.y);
    };
    BitPlane.prototype.clearAll = function () {
        for (var i = 0; i < this.data.length; i++)
            this.data[i] = 0;
    };
    BitPlane.prototype.replaceBlock = function (newBitPlane, startX, startY) {
        var width = newBitPlane.width;
        var height = newBitPlane.height;
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                //Get the new bit
                var newBit = newBitPlane.getPixelAt(x, y);
                //Update the pixel
                if (newBit)
                    this.setPixelAt(startX + x, startY + y);
                else
                    this.clearPixelAt(startX + x, startY + y);
            }
        }
    };
    BitPlane.prototype.extractBlock = function (startX, startY, width, height) {
        var block = new BitPlane(width, height);
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                //Get the new bit
                var newBit = this.getPixelAt(startX + x, startY + y);
                //Update the pixel
                if (newBit)
                    block.setPixelAt(x, y);
            }
        }
        return block;
    };
    BitPlane.prototype.drawToCanvas = function (canvasData) {
        var setColor = canvasData.setColor;
        var clearColor = canvasData.clearColor;
        var imgData = canvasData.bitplaneData;
        var data = imgData.data;
        var bitCount = this.width * this.height;
        for (var i = 0; i < bitCount; i++) {
            if (this.getPixelLinear(i)) {
                data[i * 4 + 0] = setColor[0];
                data[i * 4 + 1] = setColor[1];
                data[i * 4 + 2] = setColor[2];
            }
            else {
                data[i * 4 + 0] = clearColor[0];
                data[i * 4 + 1] = clearColor[1];
                data[i * 4 + 2] = clearColor[2];
            }
            data[i * 4 + 3] = 255;
        }
        canvasData.bitplaneCtx.putImageData(imgData, 0, 0);
    };
    BitPlane.prototype.random = function () {
        for (var x = 0; x < this.width; x++) {
            for (var y = 0; y < this.height; y++) {
                if (Math.random() > 0.5)
                    this.setPixelAt(x, y);
                else
                    this.clearPixelAt(x, y);
            }
        }
    };
    BitPlane.prototype.checkerboard = function () {
        for (var x = 0; x < this.width; x++) {
            for (var y = 0; y < this.height; y++) {
                var conjugationPattern = (x & 1) === (y & 1) ? 0 : 1;
                if (conjugationPattern)
                    this.setPixelAt(x, y);
                else
                    this.clearPixelAt(x, y);
            }
        }
    };
    BitPlane.prototype.shiftLeft = function (amount) {
        if (amount <= 0)
            return;
        if (amount > 8)
            throw new Error("Cannot shift bit plane more than 8 at a time!");
        var mask = 0xFF << (8 - amount);
        for (var i = 0; i < this.data.length; i++) {
            //Take the number out and mask it.
            //If you shift 128 by 1, it becomes 256. 
            //UInt8ClampedArray will take 256 and turn it into 255.
            var number = this.data[i];
            //Shift the first byte by 1
            number <<= amount;
            number &= 0xFF;
            this.data[i] = number;
            //Stop if this is the last byte
            if (i == this.data.length - 1)
                break;
            //Get the bits from the next byte
            var carry = (number & mask) >> amount;
            //Add them to first byte
            number |= carry;
            this.data[i] = number;
        }
    };
    return BitPlane;
}());
var EmbeddedDataBlock = /** @class */ (function (_super) {
    __extends(EmbeddedDataBlock, _super);
    function EmbeddedDataBlock(size) {
        var _this = _super.call(this, size, size) || this;
        _this.isConjugated = false;
        _this.complexity = -1;
        return _this;
    }
    EmbeddedDataBlock.fromBitPlane = function (bitplane) {
        if (bitplane.width !== bitplane.height)
            throw new Error("Bit plane dimensions not equal!");
        var eBlock = new EmbeddedDataBlock(bitplane.width);
        eBlock.data = bitplane.data.slice();
        return eBlock;
    };
    EmbeddedDataBlock.prototype.conjugate = function () {
        for (var x = 0; x < this.width; x++) {
            for (var y = 0; y < this.height; y++) {
                var bit = this.getPixelAt(x, y);
                //The conjugation pattern is just a checkboard with (0) in the topleft corner.
                var conjugationPattern = ((x & 1) === (y & 1)) ? 0 : 1;
                //Conjugation is just an XOR with that checkboard.
                if (bit === conjugationPattern)
                    this.clearPixelAt(x, y);
                else
                    this.setPixelAt(x, y);
            }
        }
        this.isConjugated = !this.isConjugated;
        this.complexity = BitPlaneComplexityDetector.AnalyzeLocalComplexity(this, 0, 0, this.width);
    };
    EmbeddedDataBlock.prototype.getComplexity = function () {
        this.complexity = BitPlaneComplexityDetector.AnalyzeLocalComplexity(this, 0, 0, this.width);
        return this.complexity;
    };
    EmbeddedDataBlock.prototype.clone = function () {
        var block = new EmbeddedDataBlock(this.width);
        block.isConjugated = this.isConjugated;
        block.complexity = this.complexity;
        block.data = this.data.slice(0);
        return block;
    };
    return EmbeddedDataBlock;
}(BitPlane));
var EmbeddedDataBlockifier = /** @class */ (function () {
    function EmbeddedDataBlockifier() {
    }
    EmbeddedDataBlockifier.BlockifyEmbeddedData = function (data, squareSize, threshold) {
        var dataBlocks = [];
        var maxEmbeddedBitIndex = squareSize * squareSize;
        //const expectedBlockCountNoFlag = (data.length * 8) / maxEmbeddedBitIndex;
        //const expectedBlockCount = expectedBlockCountNoFlag + Math.ceil(expectedBlockCountNoFlag / maxEmbeddedBitIndex);
        var expectedBlockCount = Math.ceil((data.length * 8) / (maxEmbeddedBitIndex - 1));
        var embeddedBitIndex = 1;
        var dataBitIndex = 0;
        var curByteIndex = 0;
        var curDataBlockIndex = 0;
        //Create the initial data block
        dataBlocks.push(new EmbeddedDataBlock(squareSize));
        //Go through each data byte...
        while (curByteIndex < data.length) {
            //Go through each data byte's bits
            while (dataBitIndex < 8) {
                //Get the byte's current bit into a data block
                var bitSet = this.getBitInByte(data[curByteIndex], dataBitIndex);
                //Stream that bit into the data block
                if (bitSet)
                    dataBlocks[curDataBlockIndex].setPixelLinear(embeddedBitIndex);
                embeddedBitIndex++;
                //If the block is full, make a new block
                if (embeddedBitIndex >= maxEmbeddedBitIndex) {
                    //Conjugate the block if needed
                    //Mark the first bit if it was conjugated.
                    if (dataBlocks[curDataBlockIndex].getComplexity() < threshold) {
                        dataBlocks[curDataBlockIndex].conjugate();
                        dataBlocks[curDataBlockIndex].setPixelLinear(0);
                    }
                    //Make a new block if more data needs to processed
                    //if(curByteIndex < data.length){
                    if (dataBlocks.length < expectedBlockCount) {
                        dataBlocks.push(new EmbeddedDataBlock(squareSize));
                        curDataBlockIndex++;
                    }
                    //Start at the 2nd bit of the new block.
                    //The first bit is a conjugation flag.
                    embeddedBitIndex = 1;
                }
                dataBitIndex++;
            }
            dataBitIndex = 0;
            curByteIndex++;
        }
        //Check if the last block needs to be conjugated
        if (dataBlocks[curDataBlockIndex].getComplexity() < threshold) {
            dataBlocks[curDataBlockIndex].conjugate();
            dataBlocks[curDataBlockIndex].setPixelLinear(0);
        }
        return dataBlocks;
    };
    EmbeddedDataBlockifier.CreateHeaderBlock = function (dataLength, fileExtension) {
        var headerBytes = new Array(16);
        //Initialize array to zero
        headerBytes.fill(0);
        //4-byte magic number
        headerBytes[0] = EmbeddedDataBlockifier.magicNumber.charCodeAt(0);
        headerBytes[1] = EmbeddedDataBlockifier.magicNumber.charCodeAt(1);
        headerBytes[2] = EmbeddedDataBlockifier.magicNumber.charCodeAt(2);
        headerBytes[3] = EmbeddedDataBlockifier.magicNumber.charCodeAt(3);
        //4-byte integer for file length in bytes.
        headerBytes[4] = (dataLength & 0xFF000000) >> 24;
        headerBytes[5] = (dataLength & 0x00FF0000) >> 16;
        headerBytes[6] = (dataLength & 0x0000FF00) >> 8;
        headerBytes[7] = (dataLength & 0x000000FF) >> 0;
        //8-byte ASCII Extension
        var startChar = (fileExtension.charAt(0) === ".") ? 1 : 0;
        fileExtension = fileExtension.substring(startChar, Math.min(fileExtension.length, 8));
        //let fileExtensions = fileExtension.split(".");
        //fileExtension = fileExtension.split(".",1)[1].substring(0, Math.min(fileExtension.length, 8));
        for (var i = 0; i < fileExtension.length; i++) {
            headerBytes[8 + i] = fileExtension.charCodeAt(i);
        }
        return headerBytes;
    };
    EmbeddedDataBlockifier.GetHeaderDataFromBytes = function (data) {
        if (data.length !== 16)
            throw new Error("Header Bytes incorrect size!");
        var headerMagicBytes = Array.prototype.slice.call(data, 0, 4);
        var magicNumber = String.fromCharCode.apply(String, headerMagicBytes);
        if (magicNumber !== this.magicNumber)
            throw new Error("Incorrect Header Signature!");
        //...data.slice(0,4)
        var byteCount = 0;
        byteCount |= (data[4] << 24);
        byteCount |= (data[5] << 16);
        byteCount |= (data[6] << 8);
        byteCount |= (data[7] << 0);
        var fileExtension = ".";
        for (var i = 8; i < 16; i++) {
            if (data[i] === 0)
                break;
            fileExtension += String.fromCharCode(data[i]);
        }
        if (fileExtension === ".")
            fileExtension = ".dat";
        return { magicNumber: magicNumber, byteCount: byteCount, fileExtension: fileExtension, headerLength: 16 };
    };
    EmbeddedDataBlockifier.GenerateConjugationMap = function (blocks) {
        return blocks.map(function (block) { return block.isConjugated; });
    };
    EmbeddedDataBlockifier.UnBlockifyEmbeddedDataWithoutConjugationFlag = function (incomingDataBlocks, numBytes) {
        //Clone the data
        var dataBlocks = EmbeddedDataBlockifier.deepClone(incomingDataBlocks);
        //Unconjugate blocks if needed
        dataBlocks.forEach(function (block) { if (block.isConjugated)
            block.conjugate(); });
        //Get the data out of their containers
        var blockData = dataBlocks.reduce(function (acc, cur) { return acc.concat(cur.data); }, []);
        //Merge the data into a single byte array
        blockData = blockData.reduce(function (acc, cur) { acc.push.apply(acc, cur); return acc; }, []);
        //Get the needed subarray of bytes
        if (numBytes)
            return blockData.slice(0, numBytes);
        return blockData;
    };
    EmbeddedDataBlockifier.UnBlockifyEmbeddedData = function (incomingDataBlocks, numBytes) {
        //Clone the data
        //let dataBlocks = EmbeddedDataBlockifier.deepClone(incomingDataBlocks);
        //This is a volatile operation
        var dataBlocks = incomingDataBlocks;
        //Unconjugate blocks if needed
        EmbeddedDataBlockifier.ConjugateIncomingBlocks(dataBlocks);
        var bytes = [];
        var blockBitCount = dataBlocks[0].width * dataBlocks[0].height;
        var maxBits = blockBitCount - 1;
        var curByte = 0;
        var curByteBit = 0;
        var blockIndex = 0;
        var blockBit = 1;
        //const maxBytes = Math.floor( ((incomingDataBlocks.length * blockBitCount) - (incomingDataBlocks.length)) / 8 );
        //Note that if you have 4 blocks, ignore the last 4 bits
        //Iterate through each block
        FillBytes: while (blockIndex < dataBlocks.length) {
            while (blockBit < blockBitCount) {
                //Grab the bit from this data block. Add it to the current byte.
                var bit = dataBlocks[blockIndex].getPixelLinear(blockBit);
                bytes[curByte] |= bit << (7 - curByteBit);
                curByteBit++;
                blockBit++;
                //This output byte is full. Move to the next one.
                if (curByteBit === 8) {
                    //console.log("Decoded " + String.fromCharCode(bytes[curByte]));
                    if (bytes.length === numBytes)
                        break FillBytes;
                    bytes.push(0);
                    curByte++;
                    curByteBit = 0;
                }
            }
            blockBit = 1;
            blockIndex++;
        }
        //Not enough bytes
        if (bytes.length !== numBytes)
            throw new Error("Not enough bytes were decoded!");
        var blockData = new Uint8ClampedArray(bytes);
        //Get the needed subarray of bytes
        if (numBytes)
            return blockData.slice(0, numBytes);
        return blockData;
    };
    EmbeddedDataBlockifier.ConjugateIncomingBlocks = function (dataBlocks) {
        for (var _i = 0, dataBlocks_1 = dataBlocks; _i < dataBlocks_1.length; _i++) {
            var block = dataBlocks_1[_i];
            if (block.getPixelLinear(0)) {
                block.conjugate();
            }
            //block.shiftLeft(1);
        }
    };
    EmbeddedDataBlockifier.UnBlockifyImageData = function (imgData, squareSize, threshold, minBitDepth) {
        if (squareSize === void 0) { squareSize = 8; }
        if (threshold === void 0) { threshold = 0.3; }
        if (minBitDepth === void 0) { minBitDepth = 0; }
        //Get enough blocks to get the header (16-bytes) (128 bits)
        var headerSize = 16;
        var headerBlockCount = Math.ceil((headerSize * 8) / ((squareSize * squareSize) - 1));
        var headerBlocks = EmbeddedDataBlockifier.ExtractBlocksFromBitPlanes(imgData, squareSize, threshold, minBitDepth, headerBlockCount);
        var headerData = this.UnBlockifyEmbeddedData(headerBlocks, headerSize);
        var header = this.GetHeaderDataFromBytes(headerData);
        var dataBlockCount = headerBlockCount + Math.ceil((header.byteCount * 8) / ((squareSize * squareSize) - 1));
        var secretBlocks = EmbeddedDataBlockifier.ExtractBlocksFromBitPlanes(imgData, squareSize, threshold, minBitDepth, dataBlockCount);
        var secretData = this.UnBlockifyEmbeddedData(secretBlocks, header.byteCount + header.headerLength);
        secretData = secretData.slice(headerSize);
        return { header: header, secretData: secretData };
    };
    EmbeddedDataBlockifier.ExtractBlocksFromBitPlanes = function (imgData, squareSize, threshold, minBitDepth, blockCount) {
        var blockbuffer = [];
        for (var bitDepth = 7; bitDepth >= minBitDepth; bitDepth--) {
            for (var channel = 0; channel < 3; channel++) {
                var bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
                var complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
                var cMap = complexityData.complexityMap;
                for (var i = 0; i < cMap.length; i++) {
                    var extractedBitPlane = bitplane.extractBlock(cMap[i].x * squareSize, cMap[i].y * squareSize, squareSize, squareSize);
                    var noiseblock = EmbeddedDataBlock.fromBitPlane(extractedBitPlane);
                    blockbuffer.push(noiseblock);
                    if (blockbuffer.length === blockCount) {
                        return blockbuffer;
                    }
                }
            }
        }
        if (blockbuffer.length !== blockCount)
            throw new Error("Cannot find enough blocks to extract!");
        else
            return blockbuffer;
    };
    EmbeddedDataBlockifier.getBitInByte = function (byte, position) {
        return (byte & (1 << 7 - position)) === 0 ? 0 : 1;
    };
    EmbeddedDataBlockifier.deepClone = function (dataBlocks) {
        return dataBlocks.map(function (b) { return b.clone(); });
    };
    EmbeddedDataBlockifier.magicNumber = "BPCS";
    return EmbeddedDataBlockifier;
}());
var BitPlaneCombiner = /** @class */ (function () {
    function BitPlaneCombiner(width, height) {
        this.imageData = new ImageData(width, height);
        this.makeBlack();
    }
    BitPlaneCombiner.prototype.integrateBitplane = function (bitplane, channel, bitDepth) {
        if (bitplane.width !== this.imageData.width || bitplane.height !== this.imageData.height)
            throw new Error("Bit plane dimensions do not match!");
        var data = this.imageData.data;
        var mask = 1 << (7 - bitDepth);
        for (var curSubPixel = channel, i = 0; curSubPixel < data.length; curSubPixel += 4, i++) {
            //Clear that bit and put the bitplane's bit in it.
            var bit = bitplane.getPixelLinear(i);
            data[curSubPixel] = (data[curSubPixel] & ~mask) | (bit << (7 - bitDepth));
        }
    };
    BitPlaneCombiner.prototype.makeBlack = function () {
        var data = this.imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            data[i + 0] = 0;
            data[i + 1] = 0;
            data[i + 2] = 0;
            data[i + 3] = 255;
        }
    };
    BitPlaneCombiner.prototype.makeOpaque = function () {
        var data = this.imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            //data[i + 0] = 0;
            //data[i + 1] = 0;
            //data[i + 2] = 0;
            data[i + 3] = 255;
        }
    };
    return BitPlaneCombiner;
}());
var Utilities = /** @class */ (function () {
    function Utilities() {
    }
    //Stolen from: https://stackoverflow.com/questions/3195865/converting-byte-array-to-string-in-javascript
    Utilities.string2Bin = function (str) {
        var result = [];
        for (var i = 0; i < str.length; i++) {
            result.push(str.charCodeAt(i));
        }
        return result;
    };
    Utilities.bin2String = function (array) {
        try {
            return String.fromCharCode.apply(String, array);
        }
        catch (err) {
            console.error(err);
        }
        finally {
            return "Error occurred during bin2String conversion.";
        }
    };
    Utilities.PrintEmbeddedDataBlocks = function (dataBlocks) {
        var printedBlockData = [];
        var numberBlockData = [];
        for (var _i = 0, dataBlocks_2 = dataBlocks; _i < dataBlocks_2.length; _i++) {
            var block = dataBlocks_2[_i];
            var blockData = Array.prototype.slice.call(block.data);
            for (var _a = 0, blockData_1 = blockData; _a < blockData_1.length; _a++) {
                var dataByte = blockData_1[_a];
                var bString = (dataByte).toString(2).padStart(8, "0");
                printedBlockData.push(bString);
                numberBlockData.push(dataByte);
            }
        }
        console.log(printedBlockData);
        console.log(numberBlockData);
    };
    Utilities.byteArrayToBinaryStrings = function (bytes) {
        var strings = [];
        bytes.forEach(function (x) { strings.push(x.toString(2).padStart(8, "0")); });
        return strings;
    };
    return Utilities;
}());
function downloadOutputImage() {
    var formats = {
        "png": ["image/png", ".png"],
        "jpg": ["image/jpeg", ".jpg"]
    };
    var format = formats["png"];
    bcanvas.toBlob(function (blob) {
        var dataURL = URL.createObjectURL(blob);
        var downloadBtn = document.createElement("a");
        downloadBtn.href = dataURL;
        downloadBtn.download = "encodedImage" + format[1];
        //This works in Chrome, but not Firefox.
        //downloadBtn.click();
        // create a mouse event
        var event = new MouseEvent('click');
        // dispatching it will open a save as dialog in FF
        downloadBtn.dispatchEvent(event);
        setTimeout(function () { URL.revokeObjectURL(dataURL); }, 0);
    }, format[0], 1.0);
}
function downloadExtractedData(message) {
    var file = new Blob([message.secretData]);
    var dataURL = window.URL.createObjectURL(file);
    var downloadBtn = document.createElement("a");
    downloadBtn.href = dataURL;
    downloadBtn.download = "decoded" + message.header.fileExtension;
    //This works in Chrome, but not Firefox.
    //downloadBtn.click();
    // create a mouse event
    var event = new MouseEvent('click');
    // dispatching it will open a save as dialog in FF
    downloadBtn.dispatchEvent(event);
    setTimeout(function () { URL.revokeObjectURL(dataURL); }, 0);
}
function createHeader(title, headingType) {
    if (headingType === void 0) { headingType = "h2"; }
    var header = document.createElement(headingType);
    header.innerText = title;
    header.setAttribute("style", "white-space: pre;");
    tempStorage.appendChild(header);
    //document.body.appendChild(header);
}
function clearTempStorage() {
    while (tempStorage.firstChild) {
        tempStorage.removeChild(tempStorage.firstChild);
    }
}
function DecomposeImage(imgData, squareSize, threshold, minBitDepth) {
    if (squareSize === void 0) { squareSize = 8; }
    if (threshold === void 0) { threshold = 0.3; }
    if (minBitDepth === void 0) { minBitDepth = 0; }
    //1. Turn image from Binary to Grey
    BinaryGreyConverter.binImage2greyImage(imgData);
    ctx.putImageData(imgData, 0, 0);
    //2. Turn the image into bit planes and segment it.
    //Store the extracted data.
    var extractedLayerData = [];
    //Formatting
    var colors = [[255, 0, 0, 255], [0, 255, 0, 255], [0, 0, 255, 255]];
    var highlights = ["#FFFF0080", "#FF00FF99", "#00FFFF80",];
    var channelNames = ["Red", "Green", "Blue"];
    var black = [0, 0, 0, 255];
    //Extraction
    for (var channel = 0; channel < 3; channel++) {
        createHeader(channelNames[channel] + " Channel", "h2");
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            createHeader(channelNames[channel] + " Bit Plane " + bitDepth, "h3");
            var bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
            var bitplaneCanvases = BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel]);
            var complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
            //Store the data
            extractedLayerData[channel * 8 + bitDepth] = { channel: channel, bitDepth: bitDepth, bitplane: bitplane, complexityData: complexityData, bitplaneCanvases: bitplaneCanvases };
        }
    }
    //Test: Fill the complex spots with maximum noise
    //Make the conjugation pattern (a checkerboard)
    var noise = new EmbeddedDataBlock(squareSize);
    noise.checkerboard();
    createHeader("A 100% complex noise map");
    BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(noise, black, [255, 255, 255, 255]);
    //Put the noise in the complex spots.
    for (var channel = 0; channel < 3; channel++) {
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            if (bitDepth < minBitDepth)
                continue;
            var layerData = extractedLayerData[channel * 8 + bitDepth];
            var cMap = layerData.complexityData.complexityMap;
            var bitplane = layerData.bitplane;
            var squareSize_1 = layerData.complexityData.squareSize;
            var canvases = layerData.bitplaneCanvases;
            //Redraw the canvas showing where things were overwritten
            var differenceCanvases = BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel], canvases.bitplaneCanvas);
            //Show the parts that were overwritten
            for (var i = 0; i < cMap.length; i++) {
                var ctx_1 = differenceCanvases.bitplaneCtx;
                ctx_1.fillStyle = highlights[channel]; //"rgba(0, 255, 255, 0.45)";
                ctx_1.fillRect(cMap[i].x * squareSize_1, cMap[i].y * squareSize_1, squareSize_1, squareSize_1);
            }
            //Replace complex spots with noise
            for (var i = 0; i < cMap.length; i++) {
                noise.random();
                if (noise.getComplexity() <= threshold)
                    noise.conjugate();
                bitplane.replaceBlock(noise, cMap[i].x * squareSize_1, cMap[i].y * squareSize_1);
            }
            //Redraw the canvas with noise in it
            BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel], canvases.bitplaneCanvas);
        }
    }
    //Smash the bit planes back together
    var resultImage = new BitPlaneCombiner(imgData.width, imgData.height);
    for (var channel = 0; channel < 3; channel++) {
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            var bitplane = extractedLayerData[channel * 8 + bitDepth].bitplane;
            resultImage.integrateBitplane(bitplane, channel, bitDepth);
        }
    }
    resultImage.makeOpaque();
    BinaryGreyConverter.greyImage2binImage(resultImage.imageData);
    bctx.putImageData(resultImage.imageData, 0, 0);
    //7. Turn image back from Grey to Binary
    BinaryGreyConverter.greyImage2binImage(imgData);
    ctx.putImageData(imgData, 0, 0);
}
function EncodeMessage(imgData, squareSize, threshold, minBitDepth, secretInputBytes, secretDataExtension) {
    if (squareSize === void 0) { squareSize = 8; }
    if (threshold === void 0) { threshold = 0.3; }
    if (minBitDepth === void 0) { minBitDepth = 0; }
    if (secretDataExtension === void 0) { secretDataExtension = ".txt"; }
    //1. Turn image from Binary to Grey
    ctx.putImageData(imgData, 0, 0);
    BinaryGreyConverter.binImage2greyImage(imgData);
    //2. Decompose the image into bit-planes. Find the noisy segments of the bitplanes.
    var extractedLayerData = [];
    for (var channel = 0; channel < 3; channel++) {
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            var bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
            var complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
            extractedLayerData[channel * 8 + bitDepth] = { channel: channel, bitDepth: bitDepth, bitplane: bitplane, complexityData: complexityData };
        }
    }
    //3. Group the bytes of the secret file into a series of secret blocks.
    //Conjugate the secret blocks if needed. Store a conjugation map with the blocks.
    //const secretData = ""//secretString;
    //const secretValues = string2Bin(secretData);
    var secretValues = secretInputBytes;
    //Add a header, so we know when to stop scanning and what extension this file has.
    var headerBytes = EmbeddedDataBlockifier.CreateHeaderBlock(secretValues.length, secretDataExtension);
    secretValues.unshift.apply(secretValues, headerBytes);
    //Convert the byte array into blocks.
    var secretBytes = new Uint8ClampedArray(secretValues);
    var dataBlocks = EmbeddedDataBlockifier.BlockifyEmbeddedData(secretBytes, squareSize, threshold);
    //4. Embed each secret block into the noise-like regions of the bit-planes
    //Start with LSBs of RGB, then go up in that order
    var curDataBlock = 0;
    BlockEmbedLoop: for (var bitDepth = 7; bitDepth >= minBitDepth; bitDepth--) {
        for (var channel = 0; channel < 3; channel++) {
            var layerData = extractedLayerData[channel * 8 + bitDepth];
            var bitplane = layerData.bitplane;
            var cMap = layerData.complexityData.complexityMap;
            for (var i = 0; i < cMap.length; i++) {
                //Add as many blocks to this bitplane as possible
                if (curDataBlock < dataBlocks.length) {
                    bitplane.replaceBlock(dataBlocks[curDataBlock], cMap[i].x * squareSize, cMap[i].y * squareSize);
                    curDataBlock++;
                }
                //There are no more blocks to embed. Exit the whole loop.
                else {
                    break BlockEmbedLoop;
                }
            }
        }
    }
    if (curDataBlock !== dataBlocks.length) {
        throw new Error("Not enough space to embed secret data! Over by " + (dataBlocks.length - curDataBlock) * (squareSize * squareSize / 8) + " bytes");
    }
    //Smash all the bit planes back together
    var resultImage = new BitPlaneCombiner(imgData.width, imgData.height);
    for (var channel = 0; channel < 3; channel++) {
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            var bitplane = extractedLayerData[channel * 8 + bitDepth].bitplane;
            resultImage.integrateBitplane(bitplane, channel, bitDepth);
        }
    }
    resultImage.makeOpaque();
    //5. Convert the image from CGC back to PBC.
    BinaryGreyConverter.greyImage2binImage(resultImage.imageData);
    bctx.putImageData(resultImage.imageData, 0, 0);
    //Download the stego image
    downloadOutputImage();
}
function DecodeMessage(imgData, squareSize, threshold, minBitDepth) {
    if (squareSize === void 0) { squareSize = 8; }
    if (threshold === void 0) { threshold = 0.3; }
    if (minBitDepth === void 0) { minBitDepth = 0; }
    //1. Turn image from Binary to Grey
    ctx.putImageData(imgData, 0, 0);
    BinaryGreyConverter.binImage2greyImage(imgData);
    //Extract Data from the complex blocks of the image.
    var secretData = EmbeddedDataBlockifier.UnBlockifyImageData(imgData, squareSize, threshold, minBitDepth);
    return secretData;
}
var MessageVerbosity;
(function (MessageVerbosity) {
    MessageVerbosity[MessageVerbosity["Terse"] = 0] = "Terse";
    MessageVerbosity[MessageVerbosity["Verbose"] = 1] = "Verbose";
})(MessageVerbosity || (MessageVerbosity = {}));
function AnalyzeImage(imgData, squareSize, complexity, minBitDepth, verbosity) {
    if (squareSize === void 0) { squareSize = 8; }
    if (complexity === void 0) { complexity = 0.3; }
    if (minBitDepth === void 0) { minBitDepth = 0; }
    if (verbosity === void 0) { verbosity = MessageVerbosity.Verbose; }
    var blockCount = 0;
    var uncompressedByteSize = (imgData.width * imgData.height) * 3;
    for (var channel = 0; channel < 3; channel++) {
        for (var bitDepth = 0; bitDepth < 8; bitDepth++) {
            if (bitDepth < minBitDepth)
                continue;
            var bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
            var cMap = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, complexity);
            blockCount += cMap.complexityMap.length;
        }
    }
    var maxcomplexity = 2 * squareSize * (squareSize - 1);
    var minBorderCount = Math.floor(complexity * maxcomplexity);
    var totalBytes = blockCount * (squareSize * squareSize / 8);
    var LSBSpace = (imgData.width * imgData.height) * 3 / 8;
    var sizePercentage = (totalBytes / uncompressedByteSize);
    //0%, 20%, 40%, 60%, 80%
    var spaceColors = ["#F00", "#ffa500", "#0F0", "#CCbeff", "#F0F"];
    var colorChoice = Math.min(Math.round(sizePercentage / 0.2), spaceColors.length - 1);
    var bgColor = spaceColors[colorChoice];
    console.log("%cAnalyzed Image (" + squareSize + "x" + squareSize + ", >" + (complexity * 100).toFixed(2) + "% (" + minBorderCount + "/" + maxcomplexity + "), Min Bit Depth " + minBitDepth + ")", "background: #0FF;");
    if (verbosity === MessageVerbosity.Terse) {
        //Blocks: ${squareSize}x${squareSize} | Complexity: >${(complexity * 100).toFixed(2)}% (${minBorderCount}/${maxcomplexity}) | 
        console.log("Space: " + totalBytes + " bytes (" + (totalBytes / 1024).toFixed(2) + " KiB) | " + ("%c" + (sizePercentage * 100).toFixed(2) + "%"), "background: " + bgColor + "; color: " + ((colorChoice >= (spaceColors.length - 1)) ? "#FFF" : "#000") + ";");
    }
    if (verbosity === MessageVerbosity.Verbose) {
        console.log("Dimensions " + imgData.width + "x" + imgData.height + " (" + imgData.width * imgData.height + "px)");
        console.log("Total available space: " + uncompressedByteSize + " bytes (" + (uncompressedByteSize / 1024).toFixed(2) + " KiB)");
        console.log("Minimum Bitplane Depth: " + minBitDepth);
        console.log("Total " + squareSize + "x" + squareSize + " blocks of >" + (complexity * 100).toFixed(2) + "% (" + minBorderCount + "/" + maxcomplexity + ") complexity found: " + blockCount);
        console.log("Available space: " + totalBytes + " bytes " + ("%c(" + (sizePercentage * 100).toFixed(2) + "%") + (" vessel size) (" + (totalBytes / 1024).toFixed(2) + " KiB)"), "background: " + bgColor);
        console.log("LSB storage space: " + LSBSpace + " bytes (" + (LSBSpace / uncompressedByteSize * 100).toFixed(2) + "% vessel size) (" + (LSBSpace / 1024).toFixed(2) + " KiB)");
    }
    return {
        complexity: complexity,
        squareSize: squareSize,
        blockCount: blockCount,
        totalBytes: totalBytes,
        //uncompressedByteSize,
        sizePercentage: sizePercentage,
        minBorderCount: minBorderCount,
        maxcomplexity: maxcomplexity
    };
}
function main(algorithm) {
    console.log("%cInitializing...", "background: #000; color: #FFF");
    //const blockSizeInput = document.getElementById("filename-input") as HTMLInputElement;
    //const url = blockSizeInput.value;
    var vesselFileInput = document.getElementById("vessel-file-input");
    var secretFileInput = document.getElementById("secret-file-input");
    var secretMessageInput = document.getElementById("secret-message-input");
    var secretMessage = secretMessageInput.value || "Default Secret Message";
    if (vesselFileInput.files.length !== 0) {
        var file_1 = vesselFileInput.files[0];
        var reader = new FileReader();
        reader.onload = function () {
            if (file_1.type.match('image.*')) {
                var vessel_1 = new Image();
                vessel_1.onload = function () {
                    OnVesselImageLoaded(vessel_1);
                };
                vessel_1.src = this.result;
            }
        };
        reader.onerror = function () {
            console.error("Vessel Image failed to load!");
        };
        reader.readAsDataURL(file_1);
    }
    else {
        console.warn("Please select a vessel image.");
    }
    function OnVesselImageLoaded(vessel) {
        //The secret file has been given
        if (secretFileInput.files.length !== 0) {
            var reader = new FileReader();
            reader.onload = function () {
                var arrayBuffer = this.result;
                var secretBytes = Array.prototype.slice.call(new Uint8ClampedArray(arrayBuffer));
                var secretFileName = secretFileInput.files[0].name;
                var firstDot = secretFileName.indexOf(".");
                var extension = secretFileName.substring(firstDot) || ".txt";
                ExecuteAlgorithmOnImage(vessel, algorithm, secretBytes, extension);
                secretFileInput.value = "";
            };
            reader.onerror = function () {
                console.error("Secret File Failed to load!");
                secretFileInput.value = "";
            };
            reader.readAsArrayBuffer(secretFileInput.files[0]);
        }
        //If no file given, use the secret message textarea
        else if (secretMessageInput.value.length > 0) {
            var secretBytes = Utilities.string2Bin(secretMessage);
            ExecuteAlgorithmOnImage(vessel, algorithm, secretBytes);
        }
        else {
            ExecuteAlgorithmOnImage(vessel, algorithm, []);
        }
    }
}
function ExecuteAlgorithmOnImage(img, algorithm, secretBytes, secretDataExtension) {
    canvas.width = bcanvas.width = img.width;
    canvas.height = bcanvas.height = img.height;
    ctx.drawImage(img, 0, 0);
    bctx.drawImage(img, 0, 0);
    var data = ctx.getImageData(0, 0, canvas.width, canvas.height);
    //Get inputs
    var blockSizeInput = document.getElementById("blocksize-input");
    var thresholdInput = document.getElementById("threshold-input");
    var bitdepthInput = document.getElementById("bitdepth-input");
    var parsedBlocksize = +blockSizeInput.value;
    var parsedThreshold = +thresholdInput.value;
    var parsedBitDepth = +bitdepthInput.value;
    //Minor Block Size Validation
    if (parsedBlocksize < 8) {
        parsedBlocksize = 8;
        blockSizeInput.value = "8";
    }
    var blockSizeExp = Math.log2(parsedBlocksize);
    if (blockSizeExp !== Math.floor(blockSizeExp)) {
        parsedBlocksize = Math.max(8, Math.pow(2, Math.floor(blockSizeExp)));
        blockSizeInput.value = parsedBlocksize.toString();
    }
    //Minor Complexity Validation
    if (parsedThreshold < 0) {
        parsedThreshold = 0;
        thresholdInput.value = "0";
    }
    //Measure complexity in terms of border length
    if (parsedThreshold >= 1) {
        var maxborderlength = 2 * parsedBlocksize * (parsedBlocksize - 1);
        parsedThreshold = Math.min(parsedThreshold, maxborderlength);
        thresholdInput.value = parsedThreshold.toString();
        parsedThreshold = parsedThreshold / maxborderlength;
    }
    try {
        switch (algorithm) {
            //Run the Encoding Algorithm
            case 0:
                //Empty the temp storage
                clearTempStorage();
                EncodeMessage(data, parsedBlocksize, parsedThreshold, parsedBitDepth, secretBytes, secretDataExtension);
                createHeader("Your image has been exported successfully!");
                createHeader("To decode it, use the following settings:");
                createHeader("Block Size: " + parsedBlocksize + "\nComplexity: " + thresholdInput.value + "\nMin Bit Depth: " + parsedBitDepth, "span");
                //encode(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
                break;
            //Decode a message
            case 1:
                var message = DecodeMessage(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
                downloadExtractedData(message);
                console.log(Utilities.bin2String(message.secretData));
                break;
            //Decompose an image
            case 2:
                //Empty the temp storage
                clearTempStorage();
                DecomposeImage(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
                break;
            //Analyze this image
            case 3:
                BinaryGreyConverter.binImage2greyImage(data);
                AnalyzeImage(data, parsedBlocksize, parsedThreshold, parsedBitDepth, MessageVerbosity.Verbose);
                BinaryGreyConverter.greyImage2binImage(data);
                break;
            //Run a multi-analysis
            case 4:
                BinaryGreyConverter.binImage2greyImage(data);
                var multiData = MultiAnalysis(data, parsedBitDepth, MessageVerbosity.Terse);
                ExportMultiAnalysisData(multiData);
                BinaryGreyConverter.greyImage2binImage(data);
                break;
            default: break;
        }
    }
    catch (err) {
        console.error(err);
        var error = err;
        createHeader(error.name, "h1");
        createHeader(error.message, "h2");
    }
    console.log("%cAlgorithm Complete", "background: #000; color: #FFF");
}
function ExportMultiAnalysisData(multiData) {
    multiData.sort(function (a, b) { return b.totalBytes - a.totalBytes; });
    console.dir(multiData);
    createHeader("Here's the multi-analysis data as JSON (sorted by total storage bytes).\nCopy it, and do as you will with it.");
    createHeader("Top 10 Settings", "h3");
    for (var i = 0; i < 10; i++) {
        var sizePercentage = (multiData[i].sizePercentage * 100).toFixed(2);
        var blockSize = (multiData[i].squareSize).toString().padStart(2);
        var complexity = (multiData[i].complexity * 100).toFixed(2);
        var maxcomplexity = multiData[i].maxcomplexity;
        var minBorder = multiData[i].minBorderCount;
        var settingString = "Max Storage: " + sizePercentage + "% | Block Size: " + blockSize + " | Complexity: " + complexity + "% (" + minBorder + " / " + maxcomplexity + ")\n";
        createHeader(settingString, "span");
    }
    var jsonHolder = document.createElement("textarea");
    jsonHolder.value = JSON.stringify(multiData);
    jsonHolder.setAttribute("style", "width: 640px; height: 320px");
    tempStorage.appendChild(jsonHolder);
}
function MultiAnalysis(data, minBitDepth, verbosity) {
    if (minBitDepth === void 0) { minBitDepth = 0; }
    var uncompressedByteSize = (data.width * data.height) * 3;
    console.log("Beginning Test...");
    console.log("Dimensions " + data.width + "x" + data.height + " (" + data.width * data.height + "px)");
    console.log("Total available space: " + uncompressedByteSize + " bytes (" + (uncompressedByteSize / 1024).toFixed(2) + " KiB)");
    var start = Date.now();
    var blockSizes = [8, 16, 32, 64];
    var complexityThresholds = [12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56];
    var analysisDataList = [];
    for (var _i = 0, blockSizes_1 = blockSizes; _i < blockSizes_1.length; _i++) {
        var blockSize = blockSizes_1[_i];
        console.log("%cAnalyzing Images " + blockSize + "x" + blockSize, "background: #000; color: #FFF;");
        for (var _a = 0, complexityThresholds_1 = complexityThresholds; _a < complexityThresholds_1.length; _a++) {
            var threshold = complexityThresholds_1[_a];
            var analysisData = AnalyzeImage(data, blockSize, threshold / 112, minBitDepth, verbosity);
            analysisDataList.push(analysisData);
        }
    }
    var finish = Date.now();
    console.log("Test Completed in " + (finish - start) / 1000 + " seconds");
    return analysisDataList;
}
var tempStorage = document.getElementById("temp-storage");
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var bcanvas = document.getElementById("bp-canvas");
var bctx = bcanvas.getContext("2d");
ctx.fillStyle = "#00F";
ctx.fillRect(0, 0, 1000, 1000);
bctx.fillStyle = "#F0F";
bctx.fillRect(0, 0, 1000, 1000);
