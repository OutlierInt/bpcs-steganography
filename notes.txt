A New Information Hiding Method Based on Improved BPCS Steganography (https://www.hindawi.com/journals/am/2015/698492/)
	The original BPCS method doesn't handle periodic patterns well.
	Checkerboards and stripes => 1 and 0.5 border complexity.
	This paper presents two methods to help:
	1. run-length irregularity
	2. border noisiness

High Capacity Data Hiding System Using BPCS Steganography


Other improvements
Variable complexity levels per bit plane.
	Bitplane 7 can use a lower threshold than bitplane 2.
Replace the complexity map with a bit plane for better space usage (unless we need each blocks' complexity).
	complexity of 0 for the lowest planes is nearly identical to LSB steganography

Two complexity measures are discussed
a (alpha): Border length (As shown in the BPCS Principles paper)
b (beta): number of connected areas in the block
	(Beta is not really used. It appears to peak at 0.2. Alpha is more or less uniformly distributed.)
	b = <number of connected pixel areas> / <all pixels>
	An 8x8 block with black and white vertical stripes would have b = 8/64
	A 8x8 checkerboard: b = 64/64
