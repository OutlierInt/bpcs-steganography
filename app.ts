/*
Note: This project was actually started on Nov 8th 2018, 10:53 AM

BPCS Algorithm
	1. Transform the dummy image from PBC to CGC system.
	2. Segment each bit-plane of the dummy image into informative and noise-like regions by using a threshold value (α0). A
	typical value is α0 = 0.3.
	3. Group the bytes of the secret file into a series of secret blocks.
	4. If a block (S) is less complex than the threshold (α0), then conjugate it to make it a more complex block (S*). The
	conjugated block must be more complex than α0 as shown by equation (6).
	5. Embed each secret block into the noise-like regions of the bit-planes (or, replace all the noise-like regions with a series
	of secret blocks). If the block is conjugated, then record this fact in a “conjugation map.”
	6. Also embed the conjugation map as was done with the secret blocks.
	7. Convert the embedded dummy image from CGC back to PBC.
*/
declare interface String{
	padStart(length: number, char: string);
}
declare interface Math {
	log2(x: number): number;
}
declare interface Array<T>{
	fill(value: number, start?: number, end?: number): number[];
}

class BinaryGreyConverter {
  static bin2grey(b: number) {
		//Source: Steganography in Lossy and Lossless Images, pg. 16
		return b ^ (b >> 1);
  }

  static grey2bin(g: number) {
		//Source: Steganography in Lossy and Lossless Images, pg. 16
		let bin = 0;
		while(g !== 0){
			bin ^= g;
			g >>= 1;
		}
		return bin;
  }

  static binImage2greyImage(imgData: ImageData) {
		for (let i = 0; i < imgData.data.length; i+=4){
			imgData.data[i + 0] = BinaryGreyConverter.bin2grey(imgData.data[i + 0]);
			imgData.data[i + 1] = BinaryGreyConverter.bin2grey(imgData.data[i + 1]);
			imgData.data[i + 2] = BinaryGreyConverter.bin2grey(imgData.data[i + 2]);
			//imgData[i + 3] = 255; //Don't change the alpha value.
		}
  }

  static greyImage2binImage(imgData: ImageData) {
		for (let i = 0; i < imgData.data.length; i+=4){
			imgData.data[i + 0] = BinaryGreyConverter.grey2bin(imgData.data[i + 0]);
			imgData.data[i + 1] = BinaryGreyConverter.grey2bin(imgData.data[i + 1]);
			imgData.data[i + 2] = BinaryGreyConverter.grey2bin(imgData.data[i + 2]);
			//imgData[i + 3] = 255; //Don't change the alpha value.
		}
  }
}

class BitPlaneExtractor{
	static ExtractBitPlane(imgData: ImageData, channel: number, bitNumber: number) {
		const bitplane = new BitPlane(imgData.width, imgData.height);
		const data = imgData.data;
		const mask = 1 << (7 - bitNumber);
		for(let i = channel, curPixel = 0; i < data.length; i+=4, curPixel++){
			if((data[i] & mask) !== 0){
				bitplane.setPixelLinear(curPixel);
			}
		}
		return bitplane;
	}
}

class BitPlaneComplexityDetector{
	static AnalyzeLocalComplexity(bitplane: BitPlane, blockX: number, blockY: number, squareSize: number, threshold?: number){
		//Max Horizontal complexity = (<column count> - 1) * <row count>
		//Max Vertical complexity = (<row count> - 1) * <column count>
		//Max complexity of NxN block = 2 * (N-1) * N

		//Get a block of the image
		const size = squareSize;
		const maxcomplexity = 2 * (size - 1) * size;
		blockX *= size;
		blockY *= size;

		//Get horizontal complexity
		let hComplexity = 0;
		for (let row = 0; row < size; row++) {
			let curColor = bitplane.getPixelAt(blockX + 0, blockY + row);
			for (let col = 1; col < size; col++) {
				const nextcolor = bitplane.getPixelAt(blockX + col, blockY + row);
				if (curColor !== nextcolor) hComplexity++;
				curColor = nextcolor;
			}
		}

		//Get vertical complexity
		let vComplexity = 0;
		for (let col = 0; col < size; col++) {
			let curColor = bitplane.getPixelAt(blockX + col, blockY + 0);
			for (let row = 1; row < size; row++) {
				const nextcolor = bitplane.getPixelAt(blockX + col, blockY + row);
				if (curColor !== nextcolor) vComplexity++;
				curColor = nextcolor;
			}
		}

		const complexity = (hComplexity + vComplexity) / maxcomplexity;
		return complexity;
	}

	static GenerateBitPlaneComplexityMap(bitplane: BitPlane, squareSize: number, threshold: number){
		const gridWidth  = Math.floor(bitplane.width  / squareSize);
		const gridHeight = Math.floor(bitplane.height / squareSize);

		let complexityMap: BlockData[] = [];
		for(let x = 0; x < gridWidth; x++){
			for(let y = 0; y < gridHeight; y++){
				const localComplexity = this.AnalyzeLocalComplexity(bitplane, x, y, squareSize, threshold);
				if(localComplexity >= threshold){
					complexityMap.push({x, y, complexity: localComplexity});
				}
			}
		}

		return {squareSize, minComplexity: threshold, complexityMap};
	}
}

interface ComplexityData{
	squareSize: number;
	minComplexity: number;
	complexityMap: BlockData[];
}

interface BlockData{
	x: number;
	y: number;
	complexity: number;
}

class BitPlaneCanvasGenerator{
	static createBitPlaneCanvasObjects(bitplane: BitPlane, clearColor: number[], setColor: number[], elementBefore?: Node) {
		const bitplaneCanvas = document.createElement("canvas");
		bitplaneCanvas.width = bitplane.width;
		bitplaneCanvas.height = bitplane.height;
		
		if(!elementBefore) {
			//document.body.appendChild(bitplaneCanvas);
			tempStorage.appendChild(bitplaneCanvas);
		}
		else {
			elementBefore.parentElement.insertBefore(bitplaneCanvas, elementBefore.nextSibling);
		}

		const bitplaneCtx = bitplaneCanvas.getContext("2d");
		const bitplaneData = this.createBitPlaneDrawing(bitplane, clearColor, setColor);
		bitplaneCtx.putImageData(bitplaneData, 0, 0);

		return { bitplaneData, bitplaneCanvas, bitplaneCtx, clearColor, setColor };
	}

	static createBitPlaneDrawing(bitPlane: BitPlane, clearColor: number[], setColor: number[]) {
		//Image Data has 4 8-bit channels (RGBA).
		//Split the image into 24 1-bit images.
		
		const imgData = ctx.createImageData(bitPlane.width, bitPlane.height);
		const data = imgData.data;
		for (let i = 0, px = 0; i < data.length; i+=4, px++){
			const bitSet = bitPlane.getPixelLinear(px);
			if(bitSet){
				data[i + 0] = setColor[0];
				data[i + 1] = setColor[1];
				data[i + 2] = setColor[2];
			}
			else{
				data[i + 0] = clearColor[0];
				data[i + 1] = clearColor[1];
				data[i + 2] = clearColor[2];
			}
			
			data[i + 3] = 255;
		}
		return imgData;
	}
}

interface BitPlaneCanvases { 
	bitplaneData: ImageData;
	bitplaneCanvas: HTMLCanvasElement;
	bitplaneCtx: CanvasRenderingContext2D;
	clearColor: number[];
	setColor: number[];
} 

//Represents a bit plane using an byte array
class BitPlane{
	width: number;
	height: number;
	data: Uint8ClampedArray;

	constructor(width: number, height: number){
		this.width = width;
		this.height = height;
		this.data = new Uint8ClampedArray(Math.ceil(width * height / 8));
	}

	protected boundsCheck(x: number, y: number){
		if (x < 0) throw new Error("X is less than zero!");
		if (y < 0) throw new Error("Y is less than zero!");
		if (x >= this.width)  throw new Error("X is too large!");
		if (y >= this.height) throw new Error("Y is too large!");
	}

	protected getBitIndex(x: number, y: number){
		this.boundsCheck(x, y);
		return y * (this.width) + x;
	}

	protected getByteIndex(x: number, y: number) {
		this.boundsCheck(x, y);
		return Math.floor(this.getBitIndex(x, y) / 8);
	}

	protected getBitPosition(x: number, y: number) {
		this.boundsCheck(x, y);
		const bitNumber = this.getBitIndex(x, y);
		const byteNumber = this.getByteIndex(x, y);
		return bitNumber - (byteNumber * 8);
	}

	getCoordsFromLinear(n: number){
		const y = Math.floor(n / this.width);
		const x = n - (y * this.width);
		return { x, y };
	}

	getPixelLinear(n: number){
		const coords = this.getCoordsFromLinear(n);
		return this.getPixelAt(coords.x, coords.y);
	}

	getPixelAt(x: number, y: number){
		this.boundsCheck(x, y);
		const curByte = this.data[this.getByteIndex(x, y)];
		const bitPosition = this.getBitPosition(x, y);
		const bitTest = curByte & (1 << (7 - bitPosition));
		return bitTest !== 0 ? 1 : 0;
	}

	setPixelAt(x: number, y: number){
		this.boundsCheck(x, y);
		const byteIndex = this.getByteIndex(x, y);
		const bitPosition = this.getBitPosition(x, y);
		const prevData = this.data[byteIndex];
		this.data[byteIndex] |= (1 << (7 - bitPosition));
		//console.log(`${prevData.toString(2).padStart(8, "0")} -> ${this.data[byteIndex].toString(2).padStart(8, "0")}`);
	}

	setPixelLinear(n: number){
		const coords = this.getCoordsFromLinear(n);
		return this.setPixelAt(coords.x, coords.y);
	}

	clearPixelAt(x: number, y: number){
		this.boundsCheck(x, y);
		const byteIndex = this.getByteIndex(x, y);
		const bitPosition = this.getBitPosition(x, y);
		const prevData = this.data[byteIndex];
		this.data[byteIndex] &= ~(1 << (7 - bitPosition));
		//console.log(`${prevData.toString(2).padStart(8, "0")} -> ${this.data[byteIndex].toString(2).padStart(8, "0")}`);
	}

	clearPixelLinear(n: number){
		const coords = this.getCoordsFromLinear(n);
		return this.clearPixelAt(coords.x, coords.y);
	}

	clearAll(){
		for(let i = 0; i < this.data.length; i++) this.data[i] = 0;
	}

	replaceBlock(newBitPlane: BitPlane, startX: number, startY: number){
		const width = newBitPlane.width;
		const height = newBitPlane.height;
		for(let x = 0; x < width; x++){
			for(let y = 0; y < height; y++){
				//Get the new bit
				const newBit = newBitPlane.getPixelAt(x, y);
				//Update the pixel
				if(newBit) this.setPixelAt(startX + x, startY + y);
				else this.clearPixelAt(startX + x, startY + y);
			}
		}
	}

	extractBlock(startX: number, startY: number, width: number, height: number){
		const block = new BitPlane(width, height);
		for(let x = 0; x < width; x++){
			for(let y = 0; y < height; y++){
				//Get the new bit
				const newBit = this.getPixelAt(startX + x, startY + y);
				//Update the pixel
				if (newBit) block.setPixelAt(x, y);
			}
		}
		return block;
	}

	drawToCanvas(canvasData: BitPlaneCanvases){
		const setColor = canvasData.setColor;
		const clearColor = canvasData.clearColor;
		const imgData = canvasData.bitplaneData;
		const data = imgData.data;
		const bitCount = this.width * this.height;
		for (let i = 0; i < bitCount; i++){
			if(this.getPixelLinear(i)){
				data[i * 4 + 0] = setColor[0];
				data[i * 4 + 1] = setColor[1];
				data[i * 4 + 2] = setColor[2];
			}
			else{
				data[i * 4 + 0] = clearColor[0];
				data[i * 4 + 1] = clearColor[1];
				data[i * 4 + 2] = clearColor[2];
			}
			data[i * 4 + 3] = 255;
		}

		canvasData.bitplaneCtx.putImageData(imgData, 0, 0);
	}

	random() {
		for (let x = 0; x < this.width; x++) {
			for (let y = 0; y < this.height; y++) {
				if (Math.random() > 0.5) this.setPixelAt(x, y);
				else this.clearPixelAt(x, y);
			}
		}
	}

	checkerboard() {
		for (let x = 0; x < this.width; x++) {
			for (let y = 0; y < this.height; y++) {
				const conjugationPattern = (x & 1) === (y & 1) ? 0 : 1;
				if (conjugationPattern) this.setPixelAt(x, y);
				else this.clearPixelAt(x, y);
			}
		}
	}

	shiftLeft(amount: number){
		if(amount <= 0) return;
		if(amount > 8) throw new Error("Cannot shift bit plane more than 8 at a time!");

		const mask = 0xFF << (8 - amount);
		for(let i = 0; i < this.data.length; i++){
			//Take the number out and mask it.
			//If you shift 128 by 1, it becomes 256. 
			//UInt8ClampedArray will take 256 and turn it into 255.
			let number = this.data[i];

			//Shift the first byte by 1
			number <<= amount;
			number &= 0xFF;
			this.data[i] = number;

			//Stop if this is the last byte
			if(i == this.data.length - 1) break;

			//Get the bits from the next byte
			const carry = (number & mask) >> amount;
			//Add them to first byte
			number |= carry;

			this.data[i] = number;
		}
	}
}

class EmbeddedDataBlock extends BitPlane{
	isConjugated: boolean;
	complexity: number;

	constructor(size: number){
		super(size, size);
		this.isConjugated = false;
		this.complexity = -1;
	}
	
	static fromBitPlane(bitplane: BitPlane){
		if(bitplane.width !== bitplane.height) throw new Error("Bit plane dimensions not equal!");
		const eBlock = new EmbeddedDataBlock(bitplane.width);
		eBlock.data = bitplane.data.slice();
		return eBlock;
	}

	conjugate(){
		for(let x = 0; x < this.width; x++){
			for(let y = 0; y < this.height; y++){
				const bit = this.getPixelAt(x,y);
				//The conjugation pattern is just a checkboard with (0) in the topleft corner.
				const conjugationPattern = ((x & 1) === (y & 1)) ? 0 : 1;
				//Conjugation is just an XOR with that checkboard.
				if(bit === conjugationPattern) this.clearPixelAt(x, y);
				else this.setPixelAt(x, y);
			}
		}
		this.isConjugated = !this.isConjugated;
		this.complexity = BitPlaneComplexityDetector.AnalyzeLocalComplexity(this, 0, 0, this.width);
	}

	getComplexity(){
		this.complexity = BitPlaneComplexityDetector.AnalyzeLocalComplexity(this, 0, 0, this.width);
		return this.complexity;
	}

	clone(){
		const block = new EmbeddedDataBlock(this.width);
		block.isConjugated = this.isConjugated;
		block.complexity = this.complexity;
		block.data = this.data.slice(0);
		return block;
	}
}

class EmbeddedDataBlockifier{
	static magicNumber = "BPCS";

	static BlockifyEmbeddedData(data: Uint8ClampedArray, squareSize: number, threshold: number){
		const dataBlocks: EmbeddedDataBlock[] = [];
		const maxEmbeddedBitIndex = squareSize * squareSize;
		//const expectedBlockCountNoFlag = (data.length * 8) / maxEmbeddedBitIndex;
		//const expectedBlockCount = expectedBlockCountNoFlag + Math.ceil(expectedBlockCountNoFlag / maxEmbeddedBitIndex);
		const expectedBlockCount = Math.ceil((data.length * 8) / (maxEmbeddedBitIndex - 1));

		let embeddedBitIndex = 1;
		let dataBitIndex = 0;
		let curByteIndex = 0;
		let curDataBlockIndex = 0;

		//Create the initial data block
		dataBlocks.push(new EmbeddedDataBlock(squareSize));

		//Go through each data byte...
		while(curByteIndex < data.length){
			//Go through each data byte's bits
			while(dataBitIndex < 8){
				//Get the byte's current bit into a data block
				const bitSet = this.getBitInByte(data[curByteIndex], dataBitIndex);

				//Stream that bit into the data block
				if(bitSet) dataBlocks[curDataBlockIndex].setPixelLinear(embeddedBitIndex);
				embeddedBitIndex++;
				
				//If the block is full, make a new block
				if(embeddedBitIndex >= maxEmbeddedBitIndex){
					//Conjugate the block if needed
					//Mark the first bit if it was conjugated.
					if (dataBlocks[curDataBlockIndex].getComplexity() < threshold){
						dataBlocks[curDataBlockIndex].conjugate();
						dataBlocks[curDataBlockIndex].setPixelLinear(0);
					}

					//Make a new block if more data needs to processed
					//if(curByteIndex < data.length){
					if (dataBlocks.length < expectedBlockCount){
						dataBlocks.push(new EmbeddedDataBlock(squareSize));
						curDataBlockIndex++;
					}

					//Start at the 2nd bit of the new block.
					//The first bit is a conjugation flag.
					embeddedBitIndex = 1;
				}

				dataBitIndex++;
			}
			dataBitIndex = 0;
			curByteIndex++;
		}

		//Check if the last block needs to be conjugated
		if (dataBlocks[curDataBlockIndex].getComplexity() < threshold) {
			dataBlocks[curDataBlockIndex].conjugate();
			dataBlocks[curDataBlockIndex].setPixelLinear(0);
		}

		return dataBlocks;
	}

	static CreateHeaderBlock(dataLength: number, fileExtension: string){
		const headerBytes: number[] = new Array<number>(16);
		//Initialize array to zero
		headerBytes.fill(0);

		//4-byte magic number
		headerBytes[0] = EmbeddedDataBlockifier.magicNumber.charCodeAt(0);
		headerBytes[1] = EmbeddedDataBlockifier.magicNumber.charCodeAt(1);
		headerBytes[2] = EmbeddedDataBlockifier.magicNumber.charCodeAt(2);
		headerBytes[3] = EmbeddedDataBlockifier.magicNumber.charCodeAt(3);

		//4-byte integer for file length in bytes.
		headerBytes[4] = (dataLength & 0xFF000000) >> 24;
		headerBytes[5] = (dataLength & 0x00FF0000) >> 16;
		headerBytes[6] = (dataLength & 0x0000FF00) >> 8;
		headerBytes[7] = (dataLength & 0x000000FF) >> 0;

		//8-byte ASCII Extension
		const startChar = (fileExtension.charAt(0) === ".") ? 1 : 0;
		fileExtension = fileExtension.substring(startChar, Math.min(fileExtension.length, 8));
		//let fileExtensions = fileExtension.split(".");
		//fileExtension = fileExtension.split(".",1)[1].substring(0, Math.min(fileExtension.length, 8));
		for(let i = 0; i < fileExtension.length; i++){
			headerBytes[8 + i] = fileExtension.charCodeAt(i);
		}

		return headerBytes;
	}

	static GetHeaderDataFromBytes(data: Uint8ClampedArray){
		if(data.length !== 16) throw new Error("Header Bytes incorrect size!");

		const headerMagicBytes = Array.prototype.slice.call(data, 0, 4)
		const magicNumber = String.fromCharCode(...headerMagicBytes);
		if (magicNumber !== this.magicNumber) throw new Error("Incorrect Header Signature!");
		//...data.slice(0,4)

		let byteCount = 0;
		byteCount |= (data[4] << 24);
		byteCount |= (data[5] << 16);
		byteCount |= (data[6] << 8);
		byteCount |= (data[7] << 0);

		let fileExtension = ".";
		for(let i = 8; i < 16; i++){
			if(data[i] === 0) break;
			fileExtension += String.fromCharCode(data[i]);
		}
		if(fileExtension === ".")
			fileExtension = ".dat";

		return {magicNumber, byteCount, fileExtension, headerLength: 16} as HeaderData;
	}

	static GenerateConjugationMap(blocks: EmbeddedDataBlock[]){
		return blocks.map(block => block.isConjugated);
	}

	static UnBlockifyEmbeddedDataWithoutConjugationFlag(incomingDataBlocks: EmbeddedDataBlock[], numBytes?: number){
		//Clone the data
		let dataBlocks = EmbeddedDataBlockifier.deepClone(incomingDataBlocks);
		
		//Unconjugate blocks if needed
		dataBlocks.forEach(block => { if(block.isConjugated) block.conjugate() });

		//Get the data out of their containers
		let blockData = dataBlocks.reduce((acc, cur) => { return acc.concat(cur.data); }, []);

		//Merge the data into a single byte array
		blockData = blockData.reduce((acc: any[], cur) => { acc.push(...cur); return acc; }, []);

		//Get the needed subarray of bytes
		if(numBytes) return blockData.slice(0, numBytes);
		return blockData;
	}

	static UnBlockifyEmbeddedData(incomingDataBlocks: EmbeddedDataBlock[], numBytes: number){
		//Clone the data
		//let dataBlocks = EmbeddedDataBlockifier.deepClone(incomingDataBlocks);
		
		//This is a volatile operation
		let dataBlocks = incomingDataBlocks;

		//Unconjugate blocks if needed
		EmbeddedDataBlockifier.ConjugateIncomingBlocks(dataBlocks);

		let bytes: number[] = [];

		const blockBitCount = dataBlocks[0].width * dataBlocks[0].height;
		const maxBits = blockBitCount - 1;
		let curByte = 0;
		let curByteBit = 0;
		let blockIndex = 0;
		let blockBit = 1;
		//const maxBytes = Math.floor( ((incomingDataBlocks.length * blockBitCount) - (incomingDataBlocks.length)) / 8 );

		//Note that if you have 4 blocks, ignore the last 4 bits

		//Iterate through each block
		FillBytes:
		while(blockIndex < dataBlocks.length){
			while(blockBit < blockBitCount){
				//Grab the bit from this data block. Add it to the current byte.
				const bit = dataBlocks[blockIndex].getPixelLinear(blockBit);
				bytes[curByte] |= bit << (7 - curByteBit);
				curByteBit++;
				blockBit++;

				//This output byte is full. Move to the next one.
				if(curByteBit === 8){
					//console.log("Decoded " + String.fromCharCode(bytes[curByte]));
					if(bytes.length === numBytes) break FillBytes;
					bytes.push(0);
					curByte++;
					curByteBit = 0;
				}
			}
			blockBit = 1;
			blockIndex++;
		}

		//Not enough bytes
		if(bytes.length !== numBytes)
			throw new Error("Not enough bytes were decoded!")

		let blockData: Uint8ClampedArray = new Uint8ClampedArray(bytes);

		//Get the needed subarray of bytes
		if(numBytes) return blockData.slice(0, numBytes);
		return blockData;
	}

	private static ConjugateIncomingBlocks(dataBlocks: EmbeddedDataBlock[]) {
		for (const block of dataBlocks) {
			if (block.getPixelLinear(0)) {
				block.conjugate();
			}
			//block.shiftLeft(1);
		}
	}

	static UnBlockifyImageData(imgData: ImageData, squareSize: number = 8, threshold: number = 0.3, minBitDepth: number = 0){
		//Get enough blocks to get the header (16-bytes) (128 bits)
		const headerSize = 16;
		const headerBlockCount = Math.ceil((headerSize * 8) / ((squareSize * squareSize) - 1));
		const headerBlocks: EmbeddedDataBlock[] = EmbeddedDataBlockifier.ExtractBlocksFromBitPlanes(imgData, squareSize, threshold, minBitDepth, headerBlockCount);
		const headerData = this.UnBlockifyEmbeddedData(headerBlocks, headerSize);
		const header = this.GetHeaderDataFromBytes(headerData);

		const dataBlockCount = headerBlockCount + Math.ceil((header.byteCount * 8) / ((squareSize * squareSize) - 1));
		const secretBlocks: EmbeddedDataBlock[] = EmbeddedDataBlockifier.ExtractBlocksFromBitPlanes(imgData, squareSize, threshold, minBitDepth, dataBlockCount);
		let secretData = this.UnBlockifyEmbeddedData(secretBlocks, header.byteCount + header.headerLength);
		secretData = secretData.slice(headerSize);

		return {header, secretData} as DecodedData;
	}

	private static ExtractBlocksFromBitPlanes(imgData: ImageData, squareSize: number, threshold: number, minBitDepth: number, blockCount: number) {
		const blockbuffer: EmbeddedDataBlock[] = [];
		for (let bitDepth = 7; bitDepth >= minBitDepth; bitDepth--) {
			for (let channel = 0; channel < 3; channel++) {
				const bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
				const complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
				const cMap = complexityData.complexityMap;
				for (let i = 0; i < cMap.length; i++) {
					const extractedBitPlane = bitplane.extractBlock(cMap[i].x * squareSize, cMap[i].y * squareSize, squareSize, squareSize);
					const noiseblock = EmbeddedDataBlock.fromBitPlane(extractedBitPlane);
					blockbuffer.push(noiseblock);
					if(blockbuffer.length === blockCount){
						return blockbuffer;
					}
				}
			}
		}

		if(blockbuffer.length !== blockCount)
			throw new Error("Cannot find enough blocks to extract!");
		else
			return blockbuffer;
	}

	private static getBitInByte(byte: number, position: number){
		return (byte & (1 << 7 - position)) === 0 ? 0 : 1;
	}

	private static deepClone(dataBlocks: EmbeddedDataBlock[]){
		return dataBlocks.map(b => b.clone());
	}
}

interface HeaderData {
	byteCount: number;
	fileExtension: string;
	headerLength: number;
}

class BitPlaneCombiner{
	imageData: ImageData;
	constructor(width: number, height: number){
		this.imageData = new ImageData(width, height);
		this.makeBlack();
	}

	integrateBitplane(bitplane: BitPlane, channel: number, bitDepth: number){
		if(bitplane.width !== this.imageData.width || bitplane.height !== this.imageData.height)
			throw new Error("Bit plane dimensions do not match!");

		const data = this.imageData.data;
		const mask = 1 << (7 - bitDepth);

		for(let curSubPixel = channel, i = 0; curSubPixel < data.length; curSubPixel+=4, i++){
			//Clear that bit and put the bitplane's bit in it.
			const bit = bitplane.getPixelLinear(i);
			data[curSubPixel] = (data[curSubPixel] & ~mask) | (bit << (7 - bitDepth));
		}
	}

	makeBlack(){
		const data = this.imageData.data;
		for(let i = 0; i < data.length; i+=4) {
			data[i + 0] = 0;
			data[i + 1] = 0;
			data[i + 2] = 0;
			data[i + 3] = 255;
		}
	}

	makeOpaque() {
		const data = this.imageData.data;
		for (let i = 0; i < data.length; i += 4) {
			//data[i + 0] = 0;
			//data[i + 1] = 0;
			//data[i + 2] = 0;
			data[i + 3] = 255;
		}
	}
}

class Utilities{
	//Stolen from: https://stackoverflow.com/questions/3195865/converting-byte-array-to-string-in-javascript
	static string2Bin(str: string) {
		var result: number[] = [];
		for (var i = 0; i < str.length; i++) {
			result.push(str.charCodeAt(i));
		}
		return result;
	}

	static bin2String(array) {
		try {
			return String.fromCharCode.apply(String, array);
		}
		catch (err) {
			console.error(err);
		}
		finally {
			return "Error occurred during bin2String conversion."
		}
	}

	static PrintEmbeddedDataBlocks(dataBlocks: EmbeddedDataBlock[]) {
		const printedBlockData = [];
		const numberBlockData = []
		for (const block of dataBlocks) {
			const blockData = Array.prototype.slice.call(block.data);
			for (const dataByte of blockData) {
				const bString = (dataByte).toString(2).padStart(8, "0");
				printedBlockData.push(bString);
				numberBlockData.push(dataByte);
			}
		}
		console.log(printedBlockData);
		console.log(numberBlockData);
	}

	static byteArrayToBinaryStrings(bytes: Uint8ClampedArray) {
		const strings = [];
		bytes.forEach(x => { strings.push(x.toString(2).padStart(8, "0")) });
		return strings;
	}
}

function downloadOutputImage(){
	const formats = {
		"png": ["image/png", ".png"],
		"jpg": ["image/jpeg", ".jpg"]
	}

	const format = formats["png"];
	bcanvas.toBlob(function(blob: Blob){
		const dataURL = URL.createObjectURL(blob);
		const downloadBtn = document.createElement("a");
		downloadBtn.href = dataURL;
		downloadBtn.download = "encodedImage" + format[1];
		
		//This works in Chrome, but not Firefox.
		//downloadBtn.click();

		// create a mouse event
		var event = new MouseEvent('click');
		// dispatching it will open a save as dialog in FF
		downloadBtn.dispatchEvent(event);

		setTimeout(() => { URL.revokeObjectURL(dataURL); }, 0);
	}, format[0], 1.0);
}

function downloadExtractedData(message: DecodedData){
	const file = new Blob([message.secretData]);
	const dataURL = window.URL.createObjectURL(file);
	const downloadBtn = document.createElement("a");
	downloadBtn.href = dataURL;
	downloadBtn.download = "decoded" + message.header.fileExtension;

	//This works in Chrome, but not Firefox.
	//downloadBtn.click();

	// create a mouse event
	var event = new MouseEvent('click');
	// dispatching it will open a save as dialog in FF
	downloadBtn.dispatchEvent(event);

	setTimeout(() => { URL.revokeObjectURL(dataURL); }, 0);
}

function createHeader(title: string, headingType: string = "h2") {
	const header = document.createElement(headingType);
	header.innerText = title;
	header.setAttribute("style", "white-space: pre;");
	tempStorage.appendChild(header);
	//document.body.appendChild(header);
}

function clearTempStorage() {
	while (tempStorage.firstChild) {
		tempStorage.removeChild(tempStorage.firstChild);
	}
}

function DecomposeImage(imgData: ImageData, squareSize: number = 8, threshold: number = 0.3, minBitDepth: number = 0){
	//1. Turn image from Binary to Grey
	BinaryGreyConverter.binImage2greyImage(imgData);
	ctx.putImageData(imgData, 0, 0);

	//2. Turn the image into bit planes and segment it.
	//Store the extracted data.
	const extractedLayerData: ExtractionData[] = [];

	//Formatting
	const colors = [[255, 0, 0, 255], [0, 255, 0, 255], [0, 0, 255, 255]];
	const highlights = ["#FFFF0080", "#FF00FF99", "#00FFFF80",];
	const channelNames = ["Red", "Green", "Blue"];
	const black = [0, 0, 0, 255];
	
	//Extraction
	for(let channel = 0; channel < 3; channel++){
		createHeader(`${channelNames[channel]} Channel`, "h2");
		for(let bitDepth = 0; bitDepth < 8; bitDepth++){
			createHeader(`${channelNames[channel]} Bit Plane ${bitDepth}`, "h3");
			const bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
			const bitplaneCanvases = BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel]);
			const complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
			//Store the data
			extractedLayerData[channel * 8 + bitDepth] = {channel, bitDepth, bitplane, complexityData, bitplaneCanvases};
		}
	}
	
	//Test: Fill the complex spots with maximum noise
	//Make the conjugation pattern (a checkerboard)
	const noise = new EmbeddedDataBlock(squareSize);
	noise.checkerboard();
	
	createHeader("A 100% complex noise map");
	BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(noise, black, [255,255,255,255]);

	//Put the noise in the complex spots.
	for (let channel = 0; channel < 3; channel++) {
		for (let bitDepth = 0; bitDepth < 8; bitDepth++) {
			if(bitDepth < minBitDepth) continue;
			const layerData = extractedLayerData[channel * 8 + bitDepth];
			const cMap = layerData.complexityData.complexityMap;
			const bitplane = layerData.bitplane;
			const squareSize = layerData.complexityData.squareSize;
			const canvases = layerData.bitplaneCanvases;
			
			//Redraw the canvas showing where things were overwritten
			const differenceCanvases = BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel], canvases.bitplaneCanvas);

			//Show the parts that were overwritten
			for(let i = 0; i < cMap.length; i++){
				const ctx = differenceCanvases.bitplaneCtx;
				ctx.fillStyle = highlights[channel];//"rgba(0, 255, 255, 0.45)";
				ctx.fillRect(cMap[i].x * squareSize, cMap[i].y * squareSize, squareSize, squareSize);
			}

			//Replace complex spots with noise
			for(let i = 0; i < cMap.length; i++){
				noise.random();
				if(noise.getComplexity() <= threshold) noise.conjugate();
				bitplane.replaceBlock(noise, cMap[i].x * squareSize, cMap[i].y * squareSize);
			}

			//Redraw the canvas with noise in it
			BitPlaneCanvasGenerator.createBitPlaneCanvasObjects(bitplane, black, colors[channel], canvases.bitplaneCanvas);
		}
	}

	//Smash the bit planes back together
	const resultImage = new BitPlaneCombiner(imgData.width, imgData.height);
	for (let channel = 0; channel < 3; channel++) {
		for (let bitDepth = 0; bitDepth < 8; bitDepth++) {
			const bitplane = extractedLayerData[channel * 8 + bitDepth].bitplane;
			resultImage.integrateBitplane(bitplane, channel, bitDepth);
		}
	}
	resultImage.makeOpaque();

	BinaryGreyConverter.greyImage2binImage(resultImage.imageData);
	bctx.putImageData(resultImage.imageData, 0, 0);

	//7. Turn image back from Grey to Binary
	BinaryGreyConverter.greyImage2binImage(imgData);
	ctx.putImageData(imgData, 0, 0);
}

function EncodeMessage(imgData: ImageData, squareSize: number = 8, threshold: number = 0.3, minBitDepth: number = 0, secretInputBytes: number[], secretDataExtension: string = ".txt"){
	//1. Turn image from Binary to Grey
	ctx.putImageData(imgData, 0, 0);
	BinaryGreyConverter.binImage2greyImage(imgData);

	//2. Decompose the image into bit-planes. Find the noisy segments of the bitplanes.
	const extractedLayerData: ExtractionData[] = [];
	for (let channel = 0; channel < 3; channel++) {
		for (let bitDepth = 0; bitDepth < 8; bitDepth++) {
			const bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
			const complexityData = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, threshold);
			extractedLayerData[channel * 8 + bitDepth] = { channel, bitDepth, bitplane, complexityData };
		}
	}

	//3. Group the bytes of the secret file into a series of secret blocks.
	//Conjugate the secret blocks if needed. Store a conjugation map with the blocks.
	//const secretData = ""//secretString;
	//const secretValues = string2Bin(secretData);
	const secretValues = secretInputBytes;
	
	//Add a header, so we know when to stop scanning and what extension this file has.
	const headerBytes = EmbeddedDataBlockifier.CreateHeaderBlock(secretValues.length, secretDataExtension);
	secretValues.unshift(...headerBytes);

	//Convert the byte array into blocks.
  const secretBytes = new Uint8ClampedArray(secretValues);
	const dataBlocks = EmbeddedDataBlockifier.BlockifyEmbeddedData(secretBytes, squareSize, threshold);

	//4. Embed each secret block into the noise-like regions of the bit-planes
	//Start with LSBs of RGB, then go up in that order
	let curDataBlock = 0;
	BlockEmbedLoop:
	for (let bitDepth = 7; bitDepth >= minBitDepth; bitDepth--){
		for(let channel = 0; channel < 3; channel++){
			const layerData = extractedLayerData[channel * 8 + bitDepth];
			const bitplane = layerData.bitplane;
			const cMap = layerData.complexityData.complexityMap;
			
			for(let i = 0; i < cMap.length; i++){
				//Add as many blocks to this bitplane as possible
				if(curDataBlock < dataBlocks.length){
					bitplane.replaceBlock(dataBlocks[curDataBlock], cMap[i].x * squareSize, cMap[i].y * squareSize);
					curDataBlock++;
				}
				//There are no more blocks to embed. Exit the whole loop.
				else{
					break BlockEmbedLoop;
				}
			}
		}
	}

	if(curDataBlock !== dataBlocks.length){
		throw new Error(`Not enough space to embed secret data! Over by ${(dataBlocks.length - curDataBlock) * (squareSize * squareSize / 8)} bytes`);
	}

	//Smash all the bit planes back together
	const resultImage = new BitPlaneCombiner(imgData.width, imgData.height);
	for (let channel = 0; channel < 3; channel++) {
		for (let bitDepth = 0; bitDepth < 8; bitDepth++) {
			const bitplane = extractedLayerData[channel * 8 + bitDepth].bitplane;
			resultImage.integrateBitplane(bitplane, channel, bitDepth);
		}
	}
	resultImage.makeOpaque();

	//5. Convert the image from CGC back to PBC.
	BinaryGreyConverter.greyImage2binImage(resultImage.imageData);
	bctx.putImageData(resultImage.imageData, 0, 0);

	//Download the stego image
	downloadOutputImage();
}

function DecodeMessage(imgData: ImageData, squareSize: number = 8, threshold: number = 0.3, minBitDepth: number = 0){
	//1. Turn image from Binary to Grey
	ctx.putImageData(imgData, 0, 0);
	BinaryGreyConverter.binImage2greyImage(imgData);

	//Extract Data from the complex blocks of the image.
	const secretData = EmbeddedDataBlockifier.UnBlockifyImageData(imgData, squareSize, threshold, minBitDepth);
	return secretData;
}

interface DecodedData{
	header: HeaderData;
	secretData: Uint8ClampedArray;
}

interface ExtractionData { 
	channel: number;
	bitDepth: number;
	bitplane: BitPlane;
	complexityData: ComplexityData;
	bitplaneCanvases?: BitPlaneCanvases;
}

enum MessageVerbosity{
	Terse,
	Verbose
}

function AnalyzeImage(imgData: ImageData, squareSize: number = 8, complexity: number = 0.3, minBitDepth: number = 0, verbosity: MessageVerbosity = MessageVerbosity.Verbose) {	
	let blockCount = 0;
	const uncompressedByteSize = (imgData.width * imgData.height) * 3;
	for (let channel = 0; channel < 3; channel++) {
		for (let bitDepth = 0; bitDepth < 8; bitDepth++) {
			if (bitDepth < minBitDepth) continue;
			const bitplane = BitPlaneExtractor.ExtractBitPlane(imgData, channel, bitDepth);
			const cMap = BitPlaneComplexityDetector.GenerateBitPlaneComplexityMap(bitplane, squareSize, complexity);
			blockCount += cMap.complexityMap.length;
		}
	}

	const maxcomplexity = 2 * squareSize * (squareSize - 1);
	const minBorderCount = Math.floor(complexity * maxcomplexity);
	let totalBytes = blockCount * (squareSize * squareSize / 8);
	const LSBSpace = (imgData.width * imgData.height) * 3 / 8;
	const sizePercentage = (totalBytes / uncompressedByteSize);

	//0%, 20%, 40%, 60%, 80%
	const spaceColors = ["#F00", "#ffa500", "#0F0", "#CCbeff", "#F0F"];
	const colorChoice = Math.min(Math.round(sizePercentage / 0.2), spaceColors.length - 1)
	const bgColor = spaceColors[colorChoice];

	console.log(`%cAnalyzed Image (${squareSize}x${squareSize}, >${(complexity * 100).toFixed(2)}% (${minBorderCount}/${maxcomplexity}), Min Bit Depth ${minBitDepth})`, "background: #0FF;");
	if (verbosity === MessageVerbosity.Terse) {
		//Blocks: ${squareSize}x${squareSize} | Complexity: >${(complexity * 100).toFixed(2)}% (${minBorderCount}/${maxcomplexity}) | 
		console.log(`Space: ${totalBytes} bytes (${(totalBytes / 1024).toFixed(2)} KiB) | ` + `%c${(sizePercentage * 100).toFixed(2)}%`, `background: ${bgColor}; color: ${(colorChoice >= (spaceColors.length-1)) ? "#FFF" : "#000"};`);
	}
	if(verbosity === MessageVerbosity.Verbose){
		console.log(`Dimensions ${imgData.width}x${imgData.height} (${imgData.width * imgData.height}px)`);
		console.log(`Total available space: ${uncompressedByteSize} bytes (${(uncompressedByteSize / 1024).toFixed(2)} KiB)`);
		console.log(`Minimum Bitplane Depth: ${minBitDepth}`)
		console.log(`Total ${squareSize}x${squareSize} blocks of >${(complexity * 100).toFixed(2)}% (${minBorderCount}/${maxcomplexity}) complexity found: ${blockCount}`);
		console.log(`Available space: ${totalBytes} bytes ` + `%c(${(sizePercentage * 100).toFixed(2)}%` + ` vessel size) (${(totalBytes / 1024).toFixed(2)} KiB)`, `background: ${bgColor}`);
		console.log(`LSB storage space: ${LSBSpace} bytes (${(LSBSpace / uncompressedByteSize * 100).toFixed(2)}% vessel size) (${(LSBSpace / 1024).toFixed(2)} KiB)`);
	}

	return {
		complexity,
		squareSize,
		blockCount,
		totalBytes,
		//uncompressedByteSize,
		sizePercentage,
		minBorderCount,
		maxcomplexity,
	};
}

function main(algorithm: number){
	console.log("%cInitializing...", "background: #000; color: #FFF");
	//const blockSizeInput = document.getElementById("filename-input") as HTMLInputElement;
	//const url = blockSizeInput.value;

	const vesselFileInput = document.getElementById("vessel-file-input") as HTMLInputElement;

	const secretFileInput = document.getElementById("secret-file-input") as HTMLInputElement;

	const secretMessageInput = document.getElementById("secret-message-input") as HTMLTextAreaElement;
	const secretMessage = secretMessageInput.value || "Default Secret Message";
	
	if(vesselFileInput.files.length !== 0){
		const file = vesselFileInput.files[0];
		const reader = new FileReader();
		reader.onload = function () {
			if (file.type.match('image.*')) {
				const vessel = new Image();
				vessel.onload = function(){
					OnVesselImageLoaded(vessel);
				}
				vessel.src = this.result as string;
			}
		};
		reader.onerror = function(){
			console.error("Vessel Image failed to load!");
		}
		reader.readAsDataURL(file);
	}
	else{
		console.warn("Please select a vessel image.");
	}

	function OnVesselImageLoaded(vessel: HTMLImageElement) {
		//The secret file has been given
		if (secretFileInput.files.length !== 0) {
			const reader = new FileReader();
			reader.onload = function () {
				const arrayBuffer = this.result as ArrayBuffer;
				const secretBytes: number[] = Array.prototype.slice.call(new Uint8ClampedArray(arrayBuffer));
				const secretFileName = secretFileInput.files[0].name;
				const firstDot = secretFileName.indexOf(".");
				const extension = secretFileName.substring(firstDot) || ".txt";
				ExecuteAlgorithmOnImage(vessel, algorithm, secretBytes, extension);
				secretFileInput.value = "";
			};
			reader.onerror = function(){
				console.error("Secret File Failed to load!")
				secretFileInput.value = "";
			};
			reader.readAsArrayBuffer(secretFileInput.files[0]);
		}
		//If no file given, use the secret message textarea
		else if (secretMessageInput.value.length > 0) {
			let secretBytes = Utilities.string2Bin(secretMessage);
			ExecuteAlgorithmOnImage(vessel, algorithm, secretBytes);
		}
		else {
			ExecuteAlgorithmOnImage(vessel, algorithm, []);
		}
	}
}

function ExecuteAlgorithmOnImage(img: HTMLImageElement, algorithm: number, secretBytes: number[], secretDataExtension?: string) {
	canvas.width = bcanvas.width = img.width;
	canvas.height = bcanvas.height = img.height;
	ctx.drawImage(img, 0, 0);
	bctx.drawImage(img, 0, 0);
	const data = ctx.getImageData(0, 0, canvas.width, canvas.height);
	//Get inputs
	const blockSizeInput = document.getElementById("blocksize-input") as HTMLInputElement;
	const thresholdInput = document.getElementById("threshold-input") as HTMLInputElement;
	const bitdepthInput = document.getElementById("bitdepth-input") as HTMLInputElement;
	let parsedBlocksize = +blockSizeInput.value;
	let parsedThreshold = +thresholdInput.value;
	let parsedBitDepth = +bitdepthInput.value;
	//Minor Block Size Validation
	if (parsedBlocksize < 8) {
		parsedBlocksize = 8;
		blockSizeInput.value = "8";
	}
	const blockSizeExp = Math.log2(parsedBlocksize);
	if (blockSizeExp !== Math.floor(blockSizeExp)) {
		parsedBlocksize = Math.max(8, Math.pow(2, Math.floor(blockSizeExp)));
		blockSizeInput.value = parsedBlocksize.toString();
	}
	//Minor Complexity Validation
	if (parsedThreshold < 0) {
		parsedThreshold = 0;
		thresholdInput.value = "0";
	}
	//Measure complexity in terms of border length
	if (parsedThreshold >= 1) {
		const maxborderlength = 2 * parsedBlocksize * (parsedBlocksize - 1);
		parsedThreshold = Math.min(parsedThreshold, maxborderlength);
		thresholdInput.value = parsedThreshold.toString();
		parsedThreshold = parsedThreshold / maxborderlength;
	}
	try {
		switch (algorithm) {
			//Run the Encoding Algorithm
			case 0:
				//Empty the temp storage
				clearTempStorage();
				EncodeMessage(data, parsedBlocksize, parsedThreshold, parsedBitDepth, secretBytes, secretDataExtension);
				createHeader("Your image has been exported successfully!");
				createHeader("To decode it, use the following settings:");
				createHeader(`Block Size: ${parsedBlocksize}\nComplexity: ${thresholdInput.value}\nMin Bit Depth: ${parsedBitDepth}`, "span");
				//encode(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
				break;
			//Decode a message
			case 1:
				const message = DecodeMessage(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
				downloadExtractedData(message);
				console.log(Utilities.bin2String(message.secretData));
				break;
			//Decompose an image
			case 2:
				//Empty the temp storage
				clearTempStorage();
				DecomposeImage(data, parsedBlocksize, parsedThreshold, parsedBitDepth);
				break;
			//Analyze this image
			case 3:
				BinaryGreyConverter.binImage2greyImage(data);
				AnalyzeImage(data, parsedBlocksize, parsedThreshold, parsedBitDepth, MessageVerbosity.Verbose);
				BinaryGreyConverter.greyImage2binImage(data);
				break;
			//Run a multi-analysis
			case 4:
				BinaryGreyConverter.binImage2greyImage(data);
				const multiData = MultiAnalysis(data, parsedBitDepth, MessageVerbosity.Terse);
				ExportMultiAnalysisData(multiData);
				BinaryGreyConverter.greyImage2binImage(data);
				break;
			default: break;
		}
	}
	catch (err) {
		console.error(err);
		const error = err as Error;
		createHeader(error.name, "h1");
		createHeader(error.message, "h2");
	}
	
	console.log("%cAlgorithm Complete", "background: #000; color: #FFF");
}

function ExportMultiAnalysisData(multiData: any[]) {
	multiData.sort((a, b) => b.totalBytes - a.totalBytes);
	console.dir(multiData);
	
	createHeader("Here's the multi-analysis data as JSON (sorted by total storage bytes).\nCopy it, and do as you will with it.");
	createHeader("Top 10 Settings", "h3");
	for(let i = 0; i < 10; i++){
		const sizePercentage = (multiData[i].sizePercentage * 100).toFixed(2);
		const blockSize = (multiData[i].squareSize).toString().padStart(2);
		const complexity = (multiData[i].complexity * 100).toFixed(2);
		const maxcomplexity = multiData[i].maxcomplexity;
		const minBorder = multiData[i].minBorderCount;

		const settingString = `Max Storage: ${sizePercentage}% | Block Size: ${blockSize} | Complexity: ${complexity}% (${minBorder} / ${maxcomplexity})\n`;
		createHeader(settingString, "span");
	}
	
	const jsonHolder = document.createElement("textarea");
	jsonHolder.value = JSON.stringify(multiData);
	jsonHolder.setAttribute("style", "width: 640px; height: 320px");
	tempStorage.appendChild(jsonHolder);

}

function MultiAnalysis(data: ImageData, minBitDepth: number = 0, verbosity: MessageVerbosity) {
	const uncompressedByteSize = (data.width * data.height) * 3;
	console.log("Beginning Test...");
	console.log(`Dimensions ${data.width}x${data.height} (${data.width * data.height}px)`);
	console.log(`Total available space: ${uncompressedByteSize} bytes (${(uncompressedByteSize / 1024).toFixed(2)} KiB)`);

	const start = Date.now();

	const blockSizes = [8, 16, 32, 64];
	const complexityThresholds = [12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56];
	const analysisDataList = [];
	for (const blockSize of blockSizes) {
		console.log(`%cAnalyzing Images ${blockSize}x${blockSize}`, "background: #000; color: #FFF;");
		for (const threshold of complexityThresholds) {
			const analysisData = AnalyzeImage(data, blockSize, threshold / 112, minBitDepth, verbosity);
			analysisDataList.push(analysisData);
		}
	}

	const finish = Date.now();
	console.log(`Test Completed in ${(finish - start) / 1000} seconds`);
	
	return analysisDataList;
}

const tempStorage = document.getElementById("temp-storage") as HTMLDivElement;

const canvas = document.getElementById("canvas") as HTMLCanvasElement;
const ctx = canvas.getContext("2d");

const bcanvas = document.getElementById("bp-canvas") as HTMLCanvasElement;
const bctx = bcanvas.getContext("2d");

ctx.fillStyle = "#00F";
ctx.fillRect(0, 0, 1000, 1000);
bctx.fillStyle = "#F0F";
bctx.fillRect(0, 0, 1000, 1000);
